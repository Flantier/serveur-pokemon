<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            $formData['winner_dresseur_id'] = $this->_retrieweWinner($formData);
            $fight = $this->Fights->patchEntity($fight,$formData);
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $formdata = $this->$request->getdata();
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        //enlever le finaliste du add
        //$winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'/*, 'winnerDresseurs'*/));
    }

    /**
     * Edit method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /* Supprimer la méthode edit
    public function edit($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fight = $this->Fights->patchEntity($fight, $this->request->getData());
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }*/

    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    protected function _retrieweWinner($formData) {


        $dresseurOne = $this->Dresseurs->get($drOne, [
            'contain' => []
        ]);

        $dresseurTwo = $this->Dresseurs->get($drTwo, [
            'contain' => []
        ]);
            
        $pokeOne = $this->Pokes->get($pkOne, [
            'contain' => []
        ]);

        $pokeTwo = $this->Pokes->get($pkTwo, [
            'contain' => []
        ]);
            


        $ordre = rand(0,1);
        $mort = 0;
        $PokeOneHP = $pokeOne->Health; //=pv du pokémon 1
        $PokeTwoHP = $pokeTwo->Health; //=pv du pokémon 2

        while ($mort ==0)
        {
            if ($ordre == 0 and $mort == 0)
            {

                if (($pokeOne->Attack)-($pokeTwo->Defense)>0) //en cas de défense inférieure
                {
                    $PokeTwoHP=$PokeTwoHP-(($pokeOne->Attack)-($pokeTwo->Defense));
                }
                else //si défense trop grande
                { 
                    $PokeTwoHP=$PokeTwoHP-1;
                }

                if ($PokeTwoHP<=0) {$mort=1;} //mort si plus de pv

                if ($mort == 0) //tour de l'autre joueur
                {
                    if (($pokeTwo->Attack)-($pokeOne->Defense)>0) //en cas de défense inférieure
                    {
                        $PokeOneHP=$PokeOneHP-(($pokeTwo->Attack)-($pokeOne->Defense));
                    }
                    else //si défense trop grande
                    { 
                        $PokeOneHP=$PokeOneHP-1;
                    }

                    if ($PokeOneHP<=0) {$mort=1;} //mort si plus de pv
                }

            }
            
            if ($ordre == 1 and $mort == 0)
            {
                
                if (($pokeTwo->Attack)-($pokeOne->Defense)>0) //en cas de défense inférieure
                {
                    $PokeOneHP=$PokeOneHP-(($pokeTwo->Attack)-($pokeOne->Defense));
                }
                else //si défense trop grande
                { 
                    $PokeOneHP=$PokeOneHP-1;
                }

                if ($PokeOneHP<=0) {$mort=1;} //mort si plus de pv


                if ($mort == 0) //tour de l'autre joueur
                {
                    
                    if (($pokeOne->Attack)-($pokeTwo->Defense)>0) //en cas de défense inférieure
                    {
                        $PokeTwoHP=$PokeTwoHP-(($pokeOne->Attack)-($pokeTwo->Defense));
                    }
                    else //si défense trop grande
                    { 
                        $PokeTwoHP=$PokeTwoHP-1;
                    }
    
                    if ($PokeTwoHP<=0) {$mort=1;} //mort si plus de pv

                }
            }
            
            
        }

        if ($PokeOneHP <=0) {return $dresseurOne->id; echo "Victoire du J2";}
        else {return $dresseurTwo->id; echo "Victoire du J1";}
    }



    /*public function fight($drOne, $drTwo) {

        $dresseurOne = $this->Dresseurs->get($drOne, [
            'contain' => []
        ]);

        $dresseurTwo = $this->Dresseurs->get($drTwo, [
            'contain' => []
        ]);

        $this->set(compact('dresseurOne', 'dresseurTwo'));

        echo $dresseurOne->nom;
 
        }
*/
        

}